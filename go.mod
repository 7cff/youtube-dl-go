module gitlab.com/ttpcodes/youtube-dl-go

go 1.11

require (
	github.com/sirupsen/logrus v1.3.0
	github.com/stretchr/testify v1.3.0
	golang.org/x/crypto v0.0.0-20190103213133-ff983b9c42bc // indirect
	golang.org/x/sys v0.0.0-20190109145017-48ac38b7c8cb // indirect
)
