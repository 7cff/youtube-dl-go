package youtubedl

// Represents the information about a video.
type Info struct {
	// The duration of the video, in seconds.
	Duration float64 `json:"duration"`
	// The extractor used for the video.
	Extractor string `json:"extractor"`
	// The ID of the video.
	ID string `json:"id"`
	// The title of the video.
	Title string `json:"title"`
	// The default video codec of the video, if any.
	VCodec string `json:"vcodec"`
	// The main URL the media is accessible from.
	WebpageURL string `json:"webpage_url"`
}
