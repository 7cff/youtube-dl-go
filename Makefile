all: validate test

test:
	go test -v ./test -coverpkg=gitlab.com/ttpcodes/youtube-dl-go

validate:
	go vet ./...
	staticcheck ./...

.PHONY: all test validate
