package main

import (
	"fmt"
	"gitlab.com/ttpcodes/youtube-dl-go"
)

func main() {
	// Guess what my favorite song is as of this writing?
	downloader := youtubedl.NewDownloader("https://www.youtube.com/watch?v=Qu_OzBsgRcI")
	downloader.Output("/tmp/RealOrFake")
	path, err := downloader.Run()
	if err != nil {
		fmt.Print(err)
		return
	}
	fmt.Printf("File was downloaded to %s\n", path)

	// My other favorite song, which I'll use to show channel usage:
	downloader2 := youtubedl.NewDownloader("https://www.youtube.com/watch?v=6nDrD2WNSJU")
	downloader2.Output("/tmp/" + youtubedl.ID)
	progressChan, resultChan, err := downloader2.RunProgress()
	if err != nil {
		fmt.Print(err)
	}
	for progress := range progressChan {
		fmt.Printf("Current progress: %f\n", progress)
	}
	result := <- resultChan
	if result.Err != nil {
		fmt.Print(result.Err)
		return
	}
	fmt.Printf("File 2 was downloaded to %s\n", result.Path)
}
