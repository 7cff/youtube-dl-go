// Package youtube-dl-go wraps youtube-dl's Python API.
//
// youtube-dl-go is a simple wrapper to the popular youtube-dl application for
// Python. This allows you to access the youtube-dl binary programmatically
// in any application that you write.
package youtubedl

// Represents a youtube-dl command instance for downloading a track. Do not
// instantiate this yourself. Use NewDownloader to create an instance.
type Downloader struct {
	format string
	noPlaylist bool
	output string
	url string
}

// Creates a new Downloader instance set to download the provided url.
func NewDownloader(url string) *Downloader {
	return &Downloader{
		url: url,
	}
}

// Parses the arguments of the downloader.
//
// Returns an array of strings representing the command arguments to be passed
// to `youtube-dl`.
func (d Downloader) parseArgs(action string) []string {
	args := make([]string, 0)
	if d.format != "" {
		args = append(args, "--format", d.format)
	}
	if d.noPlaylist {
		args = append(args, "--no-playlist")
	}
	if d.output != "" {
		args = append(args, "--output", d.output)
	}
	if action != "" {
		args = append(args, action)
	}
	return append(args, "--", d.url)
}

// Sets the video/audio format for the downloader.
func (d *Downloader) Format(format string) *Downloader {
	d.format = format
	return d
}

// Gets the name of the extractor that would be used to handle a given URL.
//
// URLs not matching any extractors result in a "generic" extractor being returned.
func (d Downloader) GetExtractor() (string, error) {
	return executeExtractor(d.parseArgs("--list-extractors"))
}

// Gets the info of the video, akin to calling youtube-dl with the --dump-json
// flag.
//
// This will return either an Info instance representing the video info or the
// encountered error.
func (d Downloader) GetInfo() (Info, error) {
	return executeInfo(d.parseArgs("--dump-json"))
}

// Sets the downloader to avoid downloading playlists, instead downloading the first item, if any.
func (d *Downloader) NoPlaylist() {
	d.noPlaylist = true
}

// Sets the output path for the downloader.
func (d *Downloader) Output(path string) *Downloader {
	d.output = path
	return d
}

// Runs the downloader. This method blocks until complete. For a channel-based
// solution, use RunProgress.
//
// This will return the path of the downloaded file as a string or any errors
// that were encountered.
func (d Downloader) Run() (string, error) {
	args := d.parseArgs("")
	path, err := execute(args)
	if err != nil {
		return "", err
	}
	return path, nil
}

// Runs the downloader. This method returns channels that allow to track a
// download's progress and receive the result once the download is done. For a
// simpler solution, use Run.
//
// The float64 channel returns percent progress of the download, the Result
// channel returns a Result struct representing the command's result and
// contains the command's path or any encountered errors, and the error
// indicates an error when setting up the command call.
func (d Downloader) RunProgress() (chan float64, chan Result, error) {
	args := d.parseArgs("")
	eventChan, closeChan, err := executeProgress(args)
	if err != nil {
		return nil, nil, err
	}
	return eventChan, closeChan, nil
}
