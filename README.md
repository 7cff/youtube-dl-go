# youtube-dl-go
> A Go library that wraps youtube-dl's Python API.

[![coverage report](https://gitlab.com/ttpcodes/youtube-dl-go/badges/master/coverage.svg)](https://gitlab.com/ttpcodes/youtube-dl-go/commits/master)
[![GoDoc](https://godoc.org/gitlab.com/ttpcodes/youtube-dl-go?status.svg)](https://godoc.org/gitlab.com/ttpcodes/youtube-dl-go)
[![pipeline status](https://gitlab.com/ttpcodes/youtube-dl-go/badges/master/pipeline.svg)](https://gitlab.com/ttpcodes/youtube-dl-go/commits/master)

`youtube-dl-go` is a simple wrapper to the popular 
[youtube-dl](https://github.com/rg3/youtube-dl)
application for Python. This allows you to access the `youtube-dl` binary
programmatically in any application that you write.

The motivation for creating this library was simple: many of the other pure Go
solutions for downloading YouTube videos were lacking in features or were
woefully out of date, making them difficult to use and implement in actual
projects. By wrapping `youtube-dl` with this library, we get easy access to an
already well developed download solution while also having an easy to use API
for our Go applications.

This package is still in a very alpha stage. It is not being actively
developed, as its main purpose is to provide a good `youtube-dl` interface for
my other project, [Prismriver](https://gitlab.com/ttpcodes/prismriver). That
being said however, I am more than happy to respond to feature requests to
increase the functionality of this library.

## Usage
All downloads are initiated by creating a `Downloader` instance. There is an
`Output` function which will allow you to specify where the file is 
downloaded, and supports various output templates from `youtube-dl`. Downloads
can be run by calling `Run`, which blocks the program until the download 
completes, or with `RunProgress`, which provides channels to track the 
download's progress and wait for a completed download.
```go
package main

import (
	"fmt"
	"gitlab.com/ttpcodes/youtube-dl-go"
)

func main() {
	// Guess what my favorite song is as of this writing?
	downloader := youtubedl.NewDownloader("https://www.youtube.com/watch?v=Qu_OzBsgRcI")
	downloader.Output("~/Downloads/RealOrFake")
	path, err := downloader.Run()
	if err != nil {
		fmt.Print(err)
		return
	}
	fmt.Printf("File was downloaded to %s", path)
	
	// My other favorite song, which I'll use to show channel usage:
	downloader2 := youtubedl.NewDownloader("https://www.youtube.com/watch?v=6nDrD2WNSJU")
	downloader2.Output("~/Downloads/" + youtubedl.ID)
	progressChan, resultChan, err := downloader2.RunProgress()
	if err != nil {
	    fmt.Print(err)
	}
	for progress := range progressChan {
		fmt.Printf("Current progress: %f", progress)
	}
	result := <- resultChan
	if result.Err != nil {
		fmt.Print(err)
		return
	}
	fmt.Printf("File 2 was downloaded to %s", result.Path)
}
```
When run, the program will output something similar to this:
```stdout
File was downloaded to /home/ttpcodes/Downloads/RealOrFake.mkv
Current progress: 0.000000
Current progress: 0.050000
Current progress: 0.050000
Current progress: 0.100000
Current progress: 0.250000
Current progress: 0.500000
Current progress: 1.000000
Current progress: 1.950000
Current progress: 3.950000
Current progress: 7.900000
Current progress: 15.750000
Current progress: 31.500000
... # Omitted progress lines
Current progress: 60.800000
Current progress: 71.650000
Current progress: 93.300000
Current progress: 100.000000
File 2 was downloaded to /home/ttpcodes/Downloads/6nDrD2WNSJU.mkv
```

## API
API documentation can be found on 
[GoDoc](https://godoc.org/gitlab.com/ttpcodes/youtube-dl-go).

## Installation
Any Go method of package management should support the installation of this
package. My preferred package management method is `dep`:
```bash
dep ensure gitlab.com/ttpcodes/youtube-dl-go
```

## LICENSE
This project is licensed under the
[MIT License](https://gitlab.com/ttpcodes/youtube-dl-go/blob/master/LICENSE).